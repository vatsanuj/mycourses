//
//  LessonsViewController.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import UIKit

class LessonsViewController: UITableViewController {
    

    var viewModel: LessonsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        fetchLessons()
    }
    
    private func fetchLessons() {
        viewModel?.fetchLessons { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
    private func registerTableViewCell() {
        tableView.register(UINib(nibName: "LessonTableViewCell", bundle: nil),
                           forCellReuseIdentifier: LessonTableViewCell.cellIdentifier)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.lessons.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LessonTableViewCell.cellIdentifier) as! LessonTableViewCell
        
        guard let cellModel = viewModel?.lessons[indexPath.row] else {
            return cell
        }
        
        cell.configureCell(withModel: cellModel)
        cell.delegate = self
        return cell
    }

}

extension LessonsViewController: LessonCellDelegate {
    
    func markComplete(cell: LessonTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel?.markLessonAtIndexComplete(indexPath: indexPath) {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    
    func OpenElementsForLesson(cell: LessonTableViewCell) {

        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        viewModel?.openLesson(indexPath: indexPath) {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
}
