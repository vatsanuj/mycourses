//
//  ViewController.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import UIKit
import MBProgressHUD

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passwordName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationItem.title = "Main"
    }
    
    
    @IBAction func didTapLogin(_ sender: AnyObject) {
        
        /* Add a delay to give a realisitic approach
         */
        
        ProgressHUD.showSpiner(forView: view)
        perform(#selector(goToCoursesView), with: nil, afterDelay: 0.03)
    }
    
    @objc private func goToCoursesView() {
        ProgressHUD.hideSpiner(forView: view)
        let networkService = NetworkManager()
        let courseViewModel = CoursesViewModel(networkLayer: networkService)
        let coursesViewController = CoursesTableViewController()
        coursesViewController.navigationItem.title = "Courses"
        coursesViewController.viewModel = courseViewModel
        navigationController?.pushViewController(coursesViewController, animated: true)
    }

}

