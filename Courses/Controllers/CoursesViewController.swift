//
//  TableViewController.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import UIKit
import MBProgressHUD

class CoursesTableViewController: UITableViewController {
        
    var viewModel: CoursesViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        fetchDataFromAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* It is a temporary to manage the situation
         Actual Implementation would have to use this tableViewController as the rootviewController
         and present login screen from here based on if user is logged in or not/.
        */
        
        navigationItem.hidesBackButton = true
    }
    
    fileprivate func fetchDataFromAPI() {
        ProgressHUD.showSpiner(forView: view)
        viewModel?.fetchCourses { [unowned self] in
            
            DispatchQueue.main.async {
                ProgressHUD.hideSpiner(forView: self.view)
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate func registerTableViewCell() {
        tableView.register(UINib(nibName: "CoursesTableViewCell", bundle: nil),
                           forCellReuseIdentifier: CoursesTableViewCell.identifier)
    }
    

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.courses.count ?? 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CoursesTableViewCell.identifier , for: indexPath) as! CoursesTableViewCell
        guard let course = viewModel?.courses[indexPath.row],
              let validCourse = viewModel?.getValidCourses(course: course) else {
            return cell
        }
        cell.configureCell(course: validCourse)
        return cell
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let course = viewModel?.courses[indexPath.row] else {
            return
        }
        let lessonViewController = LessonsViewController()
        let lessonViewModel = LessonsViewModel(course: course, networkService: viewModel?.networkServiceLayer)
        lessonViewController.viewModel = lessonViewModel
        lessonViewController.navigationItem.title = "Lessons"
        navigationController?.pushViewController(lessonViewController, animated: true)
    }

}
