//
//  LessonsViewModel.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation

class LessonsViewModel {
    
    private(set) var lessons: [ValidLessons] = []
    private let course: Course
    var networkServiceLayer: NetworkService?
    
    init(course: Course, networkService: NetworkService?) {
        self.course = course
        self.networkServiceLayer = networkService
    }
    
    func fetchLessons(completionHandler: @escaping () -> Void) {
        guard let url = formURLForLession() else { return }
        networkServiceLayer?.fetchLessons(url: url, completionHandler: { [weak self] (lessons) in
            guard let lessons = lessons else  { return }
            
            self?.lessons = lessons.map {  return ValidLessons(lessons: $0) }
    
            completionHandler()
        })
    }
    
    func formURLForLession() -> URL? {
        guard let id = course.id else {
            return nil
        }
        return URL(string: "https://60a8a22220a6410017305fd6.mockapi.io/v1/courses/\(id)/lessons")
    }
    
    func updateProgressForLesson(lesson: ValidLessons) {
        lesson.lessonComputedProgress = 50.0
    }
    
    func markLessonAtIndexComplete(indexPath: IndexPath, completion: @escaping () -> Void) {
        let lesson = lessons[indexPath.row]
        lesson.isCompleted = true
        lesson.lessonComputedProgress = 100.0
        completion()
    }
    
    func openLesson(indexPath: IndexPath, completion: @escaping () -> Void) {
        let lesson = lessons[indexPath.row]
        lesson.isExpanded.toggle()
        completion()
    }
    
}
