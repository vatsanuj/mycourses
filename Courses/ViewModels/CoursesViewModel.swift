//
//  CoursesViewModel.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation

class CoursesViewModel {
    
    private(set) var courses: [Course] = []
    var networkServiceLayer: NetworkService?
    let courseURL = APIEndPoints.courseEndPoint
    
    init(networkLayer: NetworkService) {
        self.networkServiceLayer = networkLayer
    }
    
    func fetchCourses(completionHandler: @escaping () -> Void) {
        guard let url = URL(string: courseURL) else { return }
        networkServiceLayer?.fetchCourses(url: url, completionHandler: { [weak self] (courses) in
            guard let courses = courses else  { return }
            self?.courses = courses
            completionHandler()
        })
    }
    
    func getValidCourses(course: Course) -> ValidCourse {
        return ValidCourse(course: course)
    }
    
}
