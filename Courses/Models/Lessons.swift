//
//  Lessons.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation

struct Lessons: Decodable {
    let id: String?
    let coursId: String?
    let name: String?
    let topic: String?
    let elements: [Element]?
}

class ValidLessons {
    
    let lessons: Lessons
    
    var isCompleted:Bool = false
    var isExpanded: Bool = false
    var lessonComputedProgress: Float = 0.0
    
    init(lessons: Lessons) {
        self.lessons = lessons
    }
}
