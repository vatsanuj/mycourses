//
//  Courses.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation


struct Course: Decodable {
    let id: String?
    let title: String?
    let subtitle: String?
    let description: String?
    let image: String?
    let lessons: [Lessons]?
}

class ValidCourse {
    
    let course: Course
    var computedProgress: Float = 0.0
    
    init(course: Course) {
        self.course = course
    }
}

