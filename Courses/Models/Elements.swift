//
//  Elements.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation

struct Element: Decodable {
    let id: String?
    let lessonId: String?
    let name: String?
    let duration: Double?
    let type: String?
    
    enum codingKeys: String, CodingKey {
        case duration = "duration_seconds"
    }
    
}
