//
//  CoursesTableViewCell.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import UIKit

class CoursesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var courseImage: UIImageView!
    @IBOutlet weak var progressiveView: UIProgressView!
    @IBOutlet weak var progressText: UILabel!
    
    static let identifier = "CoursesCell"
    
    func configureCell(course: ValidCourse) {
        titleLabel.text = course.course.title
        subtitleLabel.text = course.course.description
        /*
         Courses URL is served over Http -> Violating Apple Transport Security (ATS) Requirement
         We can disable ATS from infolist but i am avoiding it for the time being
         */
        courseImage.loadImageUsingCacheWithURLString(course.course.image ?? "", placeHolder: UIImage(named: "PlaceHolder") ) { _ in
            // If call Back Required
        }
        
        progressiveView.progress = course.computedProgress
        progressText.text = "\(course.computedProgress * 100)"
        
    }

}
