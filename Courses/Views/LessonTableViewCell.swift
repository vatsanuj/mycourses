//
//  LessionTableViewCell.swift
//  Courses
//
//  Created by Anuj Vats on 23/05/21.
//

import UIKit


protocol LessonCellDelegate {
    func OpenElementsForLesson(cell: LessonTableViewCell)
    func markComplete(cell: LessonTableViewCell)
}

class LessonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lessonTilte: UILabel!
    @IBOutlet weak var chevronImage: UIImageView!
    @IBOutlet weak var elementsView: UIStackView!
    @IBOutlet weak var lessonProgressView: UIProgressView!

    static let cellIdentifier = "LessonCell"
    
    var delegate: LessonCellDelegate?
    
    private var lessonModel: ValidLessons?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        elementsView.removeAllArrangedSubviews()
    }
    
    
    @IBAction func didTapLessons(_ sender: AnyObject) {
        delegate?.OpenElementsForLesson(cell: self)
    }

    func configureCell(withModel model: ValidLessons) {
        lessonModel = model
        SetupUI()
    }
    
    private func SetupUI() {
        lessonTilte.text = lessonModel?.lessons.name
        lessonProgressView.progress = lessonModel?.lessonComputedProgress ?? 0
        setUpElementsView()
        setImageView()
    }
    
    private func setImageView() {
        guard let viewModel = lessonModel else {
            return
        }
        
        chevronImage.image = viewModel.isExpanded ? UIImage(named: "minus") : UIImage(named: "plus")
        
    }
    
    
    func setUpElementsView() {
        guard let lessonModel = lessonModel,
              let elements = lessonModel.lessons.elements  else {
            return
        }
        
        /*
         Here Based on the type of the lesson we initialize view based on our element type
         I am doing this for a fix sample type
         */
        
        elements.forEach { (element) in
            if lessonModel.isExpanded {
                
                let elementView = setUpLessonUI(element: element)
                elementView.translatesAutoresizingMaskIntoConstraints = false
                
                elementsView.addArrangedSubview(elementView)
            }
        }
        
    }
    
    
    func setUpLessonUI(element: Element) -> UIView {
        
        let view = UIView()
        let imageView = UIImageView()
        imageView.image = UIImage(named: "VideoPlayer")
        
        let titleView = UILabel()
        titleView.text = element.name
        
        let completeButton = UIButton(type: .system)
        
        if let isCompleted = lessonModel?.isCompleted, isCompleted  {
            completeButton.setTitle("Competed", for: .normal)
            completeButton.tintColor = .green
        } else {
            completeButton.setTitle("Mark as Compete", for: .normal)
            completeButton.tintColor = .black
            completeButton.addTarget(self,
                                     action: #selector(completeButtonTap(_:)),
                                     for: .touchUpInside)
        }
        

        let seperatorView = UIView()
        seperatorView.backgroundColor = .darkGray
        
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleView.translatesAutoresizingMaskIntoConstraints = false
        completeButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(imageView)
        view.addSubview(titleView)
        view.addSubview(completeButton)
        
        NSLayoutConstraint.activate([
            
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 150),
        
            titleView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            completeButton.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20),
            completeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            completeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            completeButton.heightAnchor.constraint(equalToConstant: 50),
            completeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10)
            
        ])
        
        return view
       
    }
    
    @objc func completeButtonTap(_ sender: LessonTableViewCell) {
        delegate?.markComplete(cell: self )
    }
    
}
