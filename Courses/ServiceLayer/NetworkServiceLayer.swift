//
//  NetworkServiceLayer.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import Foundation


enum APIEndPoints {
    static let courseEndPoint = "https://60a8a22220a6410017305fd6.mockapi.io/v1/courses"
}

protocol NetworkService {
    func fetchCourses(url: URL, completionHandler: @escaping ([Course]?) -> Void)
    func fetchLessons(url: URL, completionHandler: @escaping ([Lessons]?) -> Void)
    
}

class NetworkManager: NetworkService {
    
    let urlSession = URLSession(configuration: .default)
   
    func fetchCourses(url: URL, completionHandler: @escaping ([Course]?) -> Void) {
        
        let dataTask = urlSession.dataTask(with: url) { (data, _ , _) in
            
            guard let data = data else { return }
            
            do {
                
                let courses = try JSONDecoder().decode([Course].self, from: data)
                completionHandler(courses)

            } catch let error {
                
                print("Fail to parse the data: \(error)")
            }
        }
        
        dataTask.resume()
    }
    
    func fetchLessons(url: URL, completionHandler: @escaping ([Lessons]?) -> Void) {
        
        let dataTask = urlSession.dataTask(with: url) { (data, _ , _) in
            
            guard let data = data else { return }
            
            do {
                
                let lessons = try JSONDecoder().decode([Lessons].self, from: data)
                completionHandler(lessons)

            } catch let error {
                print("Fail to parse the data: \(error)")
            }
        }
        
        dataTask.resume()
    }
    
}

