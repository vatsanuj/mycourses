//
//  ProgressHud.swift
//  Courses
//
//  Created by Anuj Vats on 22/05/21.
//

import UIKit
import MBProgressHUD

class ProgressHUD {
    
    static func showSpiner(forView view: UIView) {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    static func hideSpiner(forView view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
