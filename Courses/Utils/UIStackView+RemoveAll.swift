//
//  UIStackView+RemoveAll.swift
//  Courses
//
//  Created by Anuj Vats on 23/05/21.
//

import UIKit

extension UIStackView {
    
     func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
